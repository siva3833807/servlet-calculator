package com.calculator;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class textServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String result=(String) request.getAttribute("result"); 
		String text="";
		for(int i=0 ; i<result.length();i++)
		{
			char ch = result.charAt(i);
			switch(ch) {
			case '1':text+="one";break;
			case '2':text+="two";break;
			case '3':text+="three";break;
			case '4':text+="four";break;
			case '5':text+="five";break;
			case '6':text+="six";break;
			case '7':text+="seven";break;
			case '8':text+="eight";break;
			case '9':text+="nine";break;
			case '0':text+="zero";break;
			}
			text+=" ";
		}
		PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h3>Result: "+result+"</h3>");
        out.println("<h3>Text  : "+text+"</h3>");
        out.println("</body></html>");
				

	}


}
