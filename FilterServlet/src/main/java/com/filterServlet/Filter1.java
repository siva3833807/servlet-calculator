package com.filterServlet;

import jakarta.servlet.Filter;
import jakarta.servlet.http.HttpFilter;
import java.io.IOException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
//import jakarta.servlet.annotation.WebFilter;

//@WebFilter("/gradeServlet")
public class Filter1 extends HttpFilter implements Filter {
	private static final long serialVersionUID = 1L;


	public Filter1() {
        super();
    }


	public void destroy() {
	}


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		int s1 = Integer.parseInt(request.getParameter("s1"));
		int s2 = Integer.parseInt(request.getParameter("s2"));
		int s3 = Integer.parseInt(request.getParameter("s3"));
		String uname = request.getParameter("uname");
		
		int average = (s1+s2+s3)/3;
		System.out.println(average);
		
		request.setAttribute("name", uname);
		if (average >= 90) {
            request.setAttribute("grade", "A");
        } else if (average >= 80) {
            request.setAttribute("grade", "B");
        } else if (average >= 70) {
            request.setAttribute("grade", "C");
        } else if (average >= 60) {
            request.setAttribute("grade", "D");
        } else if (average >= 50) {
            request.setAttribute("grade", "E");
        } else {
            request.setAttribute("error", "Average should be at least 50");
            request.getRequestDispatcher("error.jsp").forward(request, response);
            return;
        }
		
		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
