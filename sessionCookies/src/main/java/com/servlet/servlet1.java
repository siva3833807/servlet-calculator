package com.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        HttpSession session = request.getSession();

        Integer count = (Integer) session.getAttribute("count");

        if (count == null) {
            count = 0;
        }

        count++;

        session.setAttribute("count", count);

        Cookie[] cookies = request.getCookies();
        Integer cookieCount = 0;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if ("count".equals(c.getName())) {
                    cookieCount = Integer.parseInt(c.getValue());
                    break;
                }
            }
        }

        cookieCount++;

        Cookie cookie = new Cookie("count", String.valueOf(cookieCount));
        cookie.setMaxAge(3600);

        response.addCookie(cookie);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        out.println("<h1>Welcome </h1><br>");
        out.println("<h2>Session Count: " + count + "</h2>");
        out.println("<h2>Cookie Count: " + cookieCount + "</h2>");
        out.println("</body></html>");
	}

}
