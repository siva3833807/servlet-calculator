# 1. servlet-calculator


# Steps:

1. **Create Servlets**: Create two servlets named `AddServlet` and `TextServlet`.

2. **Import Necessary Packages and Handle Exceptions**: Import required packages in servlets and handle exceptions if necessary.

3. **Create HTML File**: Create an HTML file named `index.html` and write a form to collect user input for the calculator.

4. **Send Request**: Use either the GET or POST method to send the request to the server. In this example, the GET method is used.

5. **Handle GET Request**: In the `AddServlet`, handle the incoming GET request by implementing the `doGet()` method.

6. **Process User Inputs**: Retrieve user inputs and arithmetic operations to be performed by the calculator.

7. **Perform Calculation**: Perform the necessary arithmetic operation and use `RequestDispatcher` to redirect the result to the 
`TextServlet`.

8. **Convert Result to Text**: In the `TextServlet`, iterate through the result and convert each character to text.

9. **Configure `web.xml`**: Configure the `web.xml` file for servlet and servlet mapping.

# 2. Session-Cookies request count


# Steps:

1. **Create Servlets**: Create a servlet named `servlet1`.

2. **Import Necessary Packages and Handle Exceptions**: Import required packages in the servlet and handle exceptions if necessary.

3. **Create HTML File**: Create an HTML file named `index.html` and write a form to send a request to the server.

4. **Send Request**: Use either the GET or POST method to send the request to the server. In this example, the GET method is used.

5. **Handle GET Request**: In the `servlet1`, handle the incoming GET request by implementing the `doGet()` method.

6. **Process User Request**: In the `doGet()` method, declare two variables, namely `count` and `cookieCount`, to count the number of requests the server receives from the client.

7. **Perform Calculation**: When the server receives a GET request, increment the counter by 1, and display the output in the browser.

8. **Configure web.xml**: Configure the `web.xml` file for servlet and servlet mapping.

